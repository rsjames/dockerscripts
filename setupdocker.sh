#!/bin/sh

CONTAINER=lz_hosted
#IMAGE=luxzeplin/offline_hosted:centos6
IMAGE=luxzeplin/offline_hosted:centos7
#IMAGE=luxzeplin/lz-event-viewer-app

# Need to remove the containner to reuse the name
# note that there is a image, which is read only and
# a container, which is the run-time object.
#..........................................................
## ALL containers ########################################
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
## Just CONTAINER ########################################
docker stop $CONTAINER
docker rm $CONTAINER

#..........................................................
## ALL images (uncomment to remove also the images) #######
docker rmi $(docker images -q)
## Just IMAGE #############################################
#docker rmi $IMAGE

#code goes in the code directory
mkdir -p code
#files go in the warehouse directory
mkdir -p warehouse

#for using UKDC
mkdir -p .globus

# NOTE—this will do nothing on OS X 
# DO NOT think to yourself, "I KNOW, let's use 'id -u' 
# This will fail by deleting everything that you mount
# with docker. INCLUDING by deleting your .ssh directory
# YOU HAVE BEEN WARNED
HOST_PASSWD_ENTRY=$(cat /etc/passwd | grep "^${USER}")
HOST_UID=$(echo ${HOST_PASSWD_ENTRY} | cut -f3 -d:)
HOST_GID=$(echo ${HOST_PASSWD_ENTRY} | cut -f4 -d:)
ENV_DIR=${HOME}/docker/env/
mkdir -p ${ENV_DIR}
cat > ${ENV_DIR}/user_hosted.env << EOF
HOST_UID=${HOST_UID}
HOST_GID=${HOST_GID}
EOF

#do some OSX setup for xforwarding
OS="$(uname -s)"
if [ ${OS} = "Darwin" ]; then
    xhost + 127.0.0.1 # X11 forwarding to host 
fi

#start up the container!

docker run -i -t -d --name $CONTAINER \
    -v /Users/Shared/cvmfs/lz.opensciencegrid.org:/cvmfs/lz.opensciencegrid.org \
    -v /Users/Shared/cvmfs/sft.cern.ch:/cvmfs/sft.cern.ch \
    -v /Users/Shared/cvmfs/dirac.egi.eu:/cvmfs/dirac.egi.eu \
    -v /Users/Shared/cvmfs/grid.cern.ch:/cvmfs/grid.cern.ch \
    -v $(pwd)/code:/home/lzuser/code \
    -v $(pwd)/warehouse:/home/lzuser/warehouse \
    -v $(pwd)/.globus:/home/lzuser/.globus \
    -v $HOME/.ssh/:/home/lzuser/.ssh \
    -e DISPLAY=host.docker.internal:0 \
    --env-file ${ENV_DIR}/user_hosted.env \
    $IMAGE

docker container ps